# Jak volat Rustovský kód na Androidu přes JNI

Popisuji zde, jak si zprovoznit kompletní prostředí pro vývoj i v Javě pro Android, i pro Rust pro Android. Pokud by bylo cílem pouze buildnout `.so` binárky z existujících Rustovských zdrojáků pro Android, lze úplně vynechat instalaci Android Studia atp., a postupovat pouze podle tutoriálu 2) (viz dále).

---

Pro zprovoznění Hello world aplikace je dobré následovat [tento tutoriál - dále jen 1)](https://mozilla.github.io/firefox-browser-architecture/experiments/2017-09-21-rust-on-android.html)

Funguje to, až na řádky `cargo --build`, které jsou poměrně důležité. Je tam asi nějaký problém s toolchainem. "Obcházím to" tak, že pro překlad rustovského `.so` používám cargo plugin [dinghy](https://github.com/snipsco/dinghy), tutoriál pro cross-compile na android [je zde - dále jen 2)](https://github.com/snipsco/dinghy/blob/master/docs/android.md). Dinghy celkově vypadá super, má smysl ho používat.

---

**Shrnutí kroků co jsou potřeba pro zprovoznění Hello world na androidu**

Následuji celý tutoriál 1) až na buildování s pomocí `cargo --build` (v závěru tutoriálu)

Následuji tutoriál 2), jenom si neinstaluji znovu NDK ale použiji tu nainstalovanou Android Studiem, tedy přeskakuji krok *[download and install Android NDK](https://developer.android.com/ndk/downloads/index.html)*. NDK jsem našel v podadresáři Android Studia `/Android/Sdk/ndk-bundle`.  Při výrobě toolchainu, tedy v kroku [build a stand-alone Android toolchain](https://developer.android.com/ndk/guides/standalone_toolchain.html#creating_the_toolchain), volím API level alespoň 21 kvůli https://github.com/snipsco/dinghy/issues/61. Jako cílovou architekturu volím armv7, tedy target `armv7-linux-androideabi`. Armv7 má HW podporu float32 aritmetiky, což určitě chceme.

Buildnu si rustovská `.so`: v adresáři `rust`

`cargo dinghy -d android build`
popř.
`cargo dinghy -d android build --release`

Udělám symlink .so do Android Studio projektu (stačí jenom jednou): z root projektu
```
cd android/app/src/main
mkdir -p jniLibs/armeabi-v7a
cd jniLibs/armeabi-v7a
ln -s ../../../../../../rust/target/armv7-linux-androideabi/release/libgreetings.so
```

Připojím telefon přes USB (v emulátoru je ARM šíleně pomalý, opravdu stojí za to mít připojený opravdový telefon), v nastavení *Developer Options* povolím *USB debugging* (bacha, občas je třeba znovu vypnout a zapnout když došlo k odpojení a připojení kabelu!). Telefon by se měl objevit i v `cargo dinghy all-devices` a v 
`adb devices -l`. Pustím na telefonu aplikaci (Shift-F10 nebo tlačítko Play), na telefonu by se mělo objevit `Hello from Rust to Android via JNI` - hotovo.
